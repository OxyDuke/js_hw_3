let userInput = parseInt(prompt("Please enter the number:"));

if (!isNaN(userInput)) {
  let i = 0;
  let hasNumbers = false;

  while (i <= userInput) {
    if (i % 5 === 0) {
      hasNumbers = true;
      console.log(i);
    }
    i++;
  }

  if (!hasNumbers) {
    console.log("Sorry, no numbers");
  }
} else {
  console.log("You entered the wrong number");
}

// Необов'язкове завдання підвищеної складності
// let userInput = parseInt(prompt("Please enter the number:"));
// if (!isNaN(userInput)) {
//     let number = parseFloat(userInput);
//  }    
//     while (!isNumber) {
//         userInput = prompt("Please enter the integer number:");
//         let parsedNumber = parseInt(userInput);
    
//         if (!isNaN(parsedNumber) && parsedNumber === parseInt(parsedNumber)) {
//             isInteger = true;
//             console.log(`The number you've entered is integer: ${parsedNumber}`);
//         } else {
//             console.log("The number you've entered is not integer. Please, try again");
//         }
//     }
    